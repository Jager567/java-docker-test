FROM frolvlad/alpine-glibc:alpine-3.8

MAINTAINER Jasper Teunissen <jasperteunissen99@gmail.com>
# Inspired by https://github.com/frol/docker-alpine-oraclejdk8/

ARG JAVA_MAJOR=10
ARG JAVA_MINOR=0
ARG JAVA_PATCH=2
ARG JAVA_BUILD=13
ARG JAVA_DL_PATH=19aef61b38124481863b1413dce1855f
ARG MAVEN_VERSION=3.5.4
ARG USER_HOME_DIR="/root"
ARG SHA=ce50b1c91364cb77efe3776f756a6d92b76d9038b0a0782f7d53acf1e997a14d
ARG BASE_URL=https://apache.osuosl.org/maven/maven-3/${MAVEN_VERSION}/binaries

ENV MAVEN_HOME=/usr/share/maven \
	MAVEN_CONFIG="$USER_HOME_DIR/.m2" \
	JAVA_HOME="/usr/lib/jvm/default-jvm"

RUN set -ex && \
	apk -U upgrade && \
	apk add --no-cache --virtual=build-dependencies \
		wget \
	&& \
	# Install requirements for running GUI applications on a server
	apk add --no-cache \
		ca-certificates \
		bash \
		xvfb \
		libxrender \
		libxtst \
		libxi \
	&& \
	wget --progress=dot "https://raw.githubusercontent.com/cjpetrus/alpine_webkit2png/master/xvfb-run" -O /usr/bin/xvfb-run && \
	chmod +x /usr/bin/xvfb-run && \
	\
	# Fix Java 10 deps
	wget --progress=dot "https://www.archlinux.org/packages/core/x86_64/zlib/download" -O /tmp/libz.tar.xz && \
	mkdir -p /tmp/libz && \
	tar -xf /tmp/libz.tar.xz -C /tmp/libz && \
	cp /tmp/libz/usr/lib/libz.so.1.2.11 /usr/glibc-compat/lib && \
	/usr/glibc-compat/sbin/ldconfig && \
	rm -rf /tmp/libz /tmp/libz.tar.xz && \
	\
	# Install Java
	wget --progress=dot:giga --header "Cookie: oraclelicense=accept-securebackup-cookie;" -O /tmp/java.tar.gz \
		http://download.oracle.com/otn-pub/java/jdk/${JAVA_MAJOR}.${JAVA_MINOR}.${JAVA_PATCH}+${JAVA_BUILD}/${JAVA_DL_PATH}/jdk-${JAVA_MAJOR}.${JAVA_MINOR}.${JAVA_PATCH}_linux-x64_bin.tar.gz && \
	\
	# Checksum isn't working yet
	#JAVA_PACKAGE_SHA256=$(curl -sSL https://www.oracle.com/webfolder/s/digest/10-0-2checksum.html | grep -E "jdk-10\.0\.2_linux-x64_bin\.tar\.gz" | grep -Eo '(sha256: )[^<]+' | cut -d: -f2 | xargs) && \
	#echo "${JAVA_PACKAGE_SHA256}  /tmp/java.tar.gz" > /tmp/java.tar.gz.sha256 && \
	#sha256sum -c /tmp/java.tar.gz.sha256 && \
	\
	tar -xzf /tmp/java.tar.gz -C /tmp && \
	mkdir -p "/usr/lib/jvm" && \
	mv "/tmp/jdk-${JAVA_MAJOR}.${JAVA_MINOR}.${JAVA_PATCH}" "/usr/lib/jvm/java-${JAVA_MAJOR}-oracle" && \
	\
	ln -s "java-${JAVA_MAJOR}-oracle" "$JAVA_HOME" && \
	ln -s "$JAVA_HOME/bin/"* "/usr/bin/" && \
	\
	# Install maven
	mkdir -p /usr/share/maven /usr/share/maven/ref && \
	wget --progress=dot:mega -O /tmp/apache-maven.tar.gz ${BASE_URL}/apache-maven-${MAVEN_VERSION}-bin.tar.gz && \
	echo "${SHA}  /tmp/apache-maven.tar.gz" | sha256sum -c - && \
	tar -xzf /tmp/apache-maven.tar.gz -C /usr/share/maven --strip-components=1 && \
	ln -s /usr/share/maven/bin/mvn /usr/bin/mvn && \
	\
	# Cleanup
	apk del build-dependencies && \
	rm -rf "$JAVA_HOME/"*src.zip \
		"$JAVA_HOME/jre/bin/javaws" \
		"$JAVA_HOME/jre/bin/jjs" \
		"$JAVA_HOME/jre/bin/keytool" \
		"$JAVA_HOME/jre/bin/orbd" \
		"$JAVA_HOME/jre/bin/pack200" \
		"$JAVA_HOME/jre/bin/policytool" \
		"$JAVA_HOME/jre/bin/rmid" \
		"$JAVA_HOME/jre/bin/rmiregistry" \
		"$JAVA_HOME/jre/bin/servertool" \
		"$JAVA_HOME/jre/bin/tnameserv" \
		"$JAVA_HOME/jre/bin/unpack200" \
		"/var/log/"* "/tmp/"* "/var/cache/apk/"* && \
	echo 'done';

COPY mvn-entrypoint.sh /usr/local/bin/mvn-entrypoint.sh

ENTRYPOINT ["/usr/local/bin/mvn-entrypoint.sh"]
CMD ["mvn"]
